import math
import pytest

@pytest.mark.parametrize('test_input',[82,78,51,66])
def test_param01(test_input):
    assert test_input > 50

@pytest.mark.parametrize('inp,out',[(2,4),(3,27),(4,256)])
def test_param02(inp,out):
    assert (inp ** inp) == out

data = [
    ([2,3,4],'sum',9),
    ([2,3,4], 'prod', 24),
]

@pytest.mark.parametrize("a,b,c",data)
def test_param03(a,b,c):
    if b=='sum':
        # b is sum here,sum of a means 2+3+4 b is the list here [2,3,4], while c is the output =9
        assert sum(a)==c
    elif b=='prod':
        #b is prod here, a=[2,3,4], c is answer=24
        assert math.prod(a)==c