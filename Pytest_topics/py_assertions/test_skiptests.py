import sys

import pytest

pytestmark=pytest.mark.skipif(sys.platform == 'darwin', reason="will run only on macos")

const = 9/5
def cent_to_fah(cent=0):
    fah = (cent * const)+32
    return fah
#print(cent_to_fah())

# Skipping a test
@pytest.mark.skip(reason="Skipping for no reason specified")
def test_case01():
    assert type(const)==float

@pytest.mark.skipif(sys.version_info>(3,6), reason="doesnot work on py version above 3.6")
def test_case02():
    assert cent_to_fah()==32

def test_case03():
    assert cent_to_fah(38) == 100.4